<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CvlangueRepository")
 */
class Cvlangue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"offreuser"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Langue", inversedBy="cvlangues")
     * @Groups({"offreuser"})
     */
    private $langue;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cv", inversedBy="cvlangues")
     * @Groups({"offreuser"})
     */
    private $cv;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLangue(): ?Langue
    {
        return $this->langue;
    }

    public function setLangue(?Langue $langue): self
    {
        $this->langue = $langue;

        return $this;
    }

    public function getCv(): ?Cv
    {
        return $this->cv;
    }

    public function setCv(?Cv $cv): self
    {
        $this->cv = $cv;

        return $this;
    }
}
