<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserOffreRepository")
 */
class UserOffre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="userOffre")
     */
    private $useroffre;



    public function __construct()
    {
        $this->useroffre = new ArrayCollection();
    }

  

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|User[]
     */
    public function getUseroffre(): Collection
    {
        return $this->useroffre;
    }

    public function addUseroffre(User $useroffre): self
    {
        if (!$this->useroffre->contains($useroffre)) {
            $this->useroffre[] = $useroffre;
            $useroffre->setUserOffre($this);
        }

        return $this;
    }

    public function removeUseroffre(User $useroffre): self
    {
        if ($this->useroffre->contains($useroffre)) {
            $this->useroffre->removeElement($useroffre);
            // set the owning side to null (unless already changed)
            if ($useroffre->getUserOffre() === $this) {
                $useroffre->setUserOffre(null);
            }
        }

        return $this;
    }

   

    
}
