<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UseroffreRepository")
 */
class Useroffre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"offreuser"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="useroffres")
     * @Groups({"offreuser"})
     */
    private $useroffre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offre", inversedBy="useroffres")
     * @Groups({"offreuser"})
     */
    private $userof;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUseroffre(): ?User
    {
        return $this->useroffre;
    }

    public function setUseroffre(?User $useroffre): self
    {
        $this->useroffre = $useroffre;

        return $this;
    }

    public function getUserof(): ?Offre
    {
        return $this->userof;
    }

    public function setUserof(?Offre $userof): self
    {
        $this->userof = $userof;

        return $this;
    }

  
}
