<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *  @Groups({"show"})
     */
    private $id;

     /**
     * @ORM\Column(type="string", nullable=true)
     *  @Groups({"show"})
    */
    private $adresse;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *  @Groups({"show"})
     */
    private $civilite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *  @Groups({"show"})
     */
    private $commentaire;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *  @Groups({"show"})
     */
    private $matricule;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show"})
     */
    private $nomcomplet;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show"})
     */
    private $mobile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Compte", mappedBy="client")
     * @Groups({"show"})
     */
    private $comptes;

    public function __construct()
    {
        $this->comptes = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }


    public function getAdresse(): ?String
    {
        return $this->adresse;
    }

    public function setAdresse(?String $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(?string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    public function setMatricule(?string $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getNomcomplet(): ?string
    {
        return $this->nomcomplet;
    }

    public function setNomcomplet(string $nomcomplet): self
    {
        $this->nomcomplet = $nomcomplet;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * @return Collection|Compte[]
     */
    public function getComptes(): Collection
    {
        return $this->comptes;
    }

    public function addCompte(Compte $compte): self
    {
        if (!$this->comptes->contains($compte)) {
            $this->comptes[] = $compte;
            $compte->setClient($this);
        }

        return $this;
    }

    public function removeCompte(Compte $compte): self
    {
        if ($this->comptes->contains($compte)) {
            $this->comptes->removeElement($compte);
            // set the owning side to null (unless already changed)
            if ($compte->getClient() === $this) {
                $compte->setClient(null);
            }
        }

        return $this;
    }



}
