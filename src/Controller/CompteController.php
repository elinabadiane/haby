<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Compte;
use App\Form\CompteType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 *  @Route("/api")
 */

class CompteController extends FOSRestController
{
    /**
     * @Route("/security/compte", name="compte", methods={"POST"})
     * @IsGranted({"ROLE_RESPONSABLE"}, statusCode=403, message="Vous n'avez pas accès à cette page !" )
     */
    public function index(Request $request,EntityManagerInterface $manager)
    {
        $compte = new Compte();
        $connecte=$this->getUser();
        $user = $this->getDoctrine()->getRepository(User::class)->find($connecte);
        $user->addCompte($compte);
        $munite = date('m');
        $seconde = date('s');
        $tata= date('k');
        $numerocompte=$tata.$munite.$seconde;
        $compte->setNumerocompte($numerocompte);
        $compte->setDatecreation(new \DateTime());
        $compte->setSolde(0);
        $form = $this->createForm(CompteType::class, $compte);
        $data=$request->request->all();
        $form->submit($data);
$manager = $this->getDoctrine()->getManager();
$manager->persist($compte);
$manager->flush();
return new Response('Les informations du compte ont été ajouté',Response::HTTP_CREATED); 

    }
}
